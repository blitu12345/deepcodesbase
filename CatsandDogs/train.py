import numpy as np
import os
import pandas as pd
import argparse

from keras.callbacks import ModelCheckpoint
from os.path import join
from vis import visualize
from dataset import dataset
from convnet import Model
from keras.optimizers import RMSprop
from keras.models import model_from_json

# parsing arguments
parser = argparse.ArgumentParser()
parser.add_argument("--net", default="VGG16", help="which nets to use-VGG16 or net_lecun", nargs='?')
parser.add_argument("--channels", default=3, type=int, help="image channel")
parser.add_argument("--qty", default=None, type=int, help="2*quantity of data required", nargs='?')
parser.add_argument("--width", type=int, help="width of image", default=64, nargs='?')
parser.add_argument("--height", type=int, help="height of image", default=64, nargs='?')
parser.add_argument("--batch_size", type=int, help="size of batch", default=5, nargs='?')
parser.add_argument("--epoch", type=int, help="number of epoch", default=3, nargs='?')
parser.add_argument("--validation", type=float, help="% of train for validation", default=0.1, nargs='?')
parser.add_argument("--vis", action='store_true', help="True for visualization")
parser.add_argument("--test", action='store_true', help="true for test submission file")
parser.add_argument("--save_model", action='store_true', help="to save a model True")
parser.add_argument("--train", action='store_true', help="to train a model")
args = parser.parse_args()

data = dataset(imgH=args.height, imgC=args.width)
print "collecting data"

if args.train:
    train_x, train_y = data.get_data(qty=args.qty)
    print "train shape\n", train_x.shape

# TODO: put assert to check for train data size in corresponds to model input

if args.net == "VGG16":
    model = Model(channels=args.channels, width=args.width, height=args.height).VGG16()
else:
    model = Model(channels=args.channels, width=args.width, height=args.height).net_lecun()

print "model summary\n"
print model.summary()

if args.save_model:
    print("saving model architecture\n")
    # save model blueprints
    model_json = model.to_json()
    model_architecture = args.net + ".json"
    with open(model_architecture, "w") as json_file:
        json_file.write(model_json)

# training model
print "training model \n"
checkpoint = ModelCheckpoint(filepath="model_best.hdf5", monitor='val_loss', verbose=1, save_best_only=True,\
                             save_weights_only=True, mode='min', period=1)
# if args.augmentation == True:
#     print("augmenting and fitting data\n")
#     break
#     train_size = 2*(args.qty - int(args.qty*0.05))
#     val_size = 2*int(args.qty*0.05)
#     history = model.fit_generator(datagen.flow(train_x[: train_size], train_y[:train_size],
#  batch_size=args.batch_size),\
#                                   validation_data=(train_x[train_size:], train_y[train_size:]), \
#                                   epochs=args.epoch, steps_per_epoch=train_size/args.batch_size, \
#                                   validation_steps=(val_size/args.batch_size+1), callbacks=[checkpoint])
#
# else:
if args.train:
    history = model.fit(train_x, train_y, validation_split=args.validation, batch_size=args.batch_size,\
                    nb_epoch=args.epoch, verbose=1, callbacks=[checkpoint])
    print "summary", history.history.keys()

if args.test:
    model_architecture = args.net + ".json"
    model_json_file = open(model_architecture, 'r')
    model_json = model_json_file.read()
    model = model_from_json(model_json)
    model_json_file.close()

    model.load_weights("model_best.hdf5")

    model.compile(loss='binary_crossentropy',
                  optimizer=RMSprop(lr=1e-4),
                  metrics=['accuracy']
                  )

    id, xtest = data.get_data(data='test')
    y_pred = model.predict(xtest)
    y_pred = y_pred.reshape(len(y_pred))
    print "y_pred", len(y_pred)
    print "id ", len(id)
    df = pd.DataFrame({'id': id, 'label': y_pred})
    df.to_csv("sub1.csv", index=False)


print "visualize the graph1"
visualize(history, "loss", "val_loss")
