from random import shuffle
import numpy as np
import cv2
import os
from os.path import join
from keras.preprocessing.image import ImageDataGenerator


class dataset():

    def __init__(self, imgH=64, imgC=64):
        self.data_path = os.getcwd()
        self.train_data_path = join(self.data_path, "data/train")
        self.test_data_path = join(self.data_path, "data/test")
        self.imgH = imgH
        self.imgC = imgC
        self.count = 0
        if len(os.listdir(self.train_data_path)) > 2:
            print "creating training database"
            self.create_data(self.train_data_path)

    def preprocess(self, image):
        if self.count % 5000 == 0:
            print "{} images done".format(self.count)
        try:
            img = cv2.imread(image)
            img = cv2.resize(img, (self.imgC, self.imgH), interpolation=cv2.INTER_LINEAR) / 255.0
            self.count += 1
            return img.reshape(self.imgC, self.imgH, 3)
        except:
            print "exception for image", image
            raise

    def image_save(self, image=None, img_path=None, store_path=None):
        try:
            img = cv2.imread(join(img_path, image))
            cv2.imwrite(join(store_path, image), img)
        except:
            print "exception for image", image, "in", img_path

        return

    def create_data(self, data_path=None):
        try:
            os.system('mkdir {}'.format(join(data_path, "cats")))
            os.system('mkdir {}'.format(join(data_path, "dogs")))
        except:
            print "directory already exist"
        images = os.listdir(data_path)
        for i, image in enumerate(images):
            if image.split('.')[-1] in ['jpg', 'png', 'jpeg']:
                if i % 100:
                    print "adding images to", data_path
                    print "{} completed".format(i)
                    print "{} remaining".format(len(images) - i)
                if "dog" in image:
                    self.image_save(image, data_path, join(data_path, "dogs"))
                    os.system('rm -rf {}'.format(join(data_path, image)))
                    continue
                self.image_save(image, data_path, join(data_path, "cats"))
                os.system('rm -rf {}'.format(join(data_path, image)))
        return

    def get_data(self, data="train", qty=None):

        if data == "train":
            print "creating training data "
            cat_path = join(self.train_data_path, 'cats')
            dog_path = join(self.train_data_path, "dogs")

            print "shuffling data"
            cats = os.listdir(cat_path)
            shuffle(cats)
            dogs = os.listdir(dog_path)
            shuffle(dogs)

            print "getting path of images"
            cats = [join(cat_path, cat) for cat in cats[:len(cats) if qty == None else qty]]
            dogs = [join(dog_path, dog) for dog in dogs[:len(dogs) if qty == None else qty]]

            training_data = map(self.preprocess, cats + dogs)
            label = [0] * (len(dogs) if qty == None else qty) + [1] * (len(cats) if qty == None else qty)
            return np.array(training_data), np.array(label)

        elif data == 'test':
            print "creating test data"
            testid = []
            images = []#[join(self.test_data_path, image) for image in os.listdir(self.test_data_path)[:qty]]
            for image in os.listdir(self.test_data_path)[:qty]:
                images.append(join(self.test_data_path, image))
                testid.append(image.split('.')[0])
            images = map(self.preprocess, images)
            return testid, np.array(images)


if __name__ == "__main__":
    dt = dataset()
    test = dt.get_data(data='test', qty=20)
    print "test shape", test.shape
    print test[0]
