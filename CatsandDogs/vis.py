import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

def visualize(history=None, *args):

    plt.figure(figsize=(15, 10))
    colors = ['r', 'g', 'b', 'y']
    title = ""
    for color, arg in zip(colors, args):
        plt.plot(history.history[arg], label=str(arg), color=color)
        title += str(arg) + ","
    plt.xlabel("iterations")
    plt.title("plot of {}".format(title))
    plt.legend(loc=9, prop={'size': 8})
    plt.savefig('learning_curve')

    return None

