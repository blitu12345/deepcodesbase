import tensorflow as tf
import numpy as np

X = np.random.rand(4, 3).astype('float32')
Y = np.random.rand(4, 1).astype('float32')

W = tf.Variable(tf.random_normal([3, 1]))


def sigmoid(x):
    return tf.nn.sigmoid(x)


pred = tf.matmul(X, W)
error = tf.subtract(Y, pred)
mse = tf.reduce_mean(tf.square(error))

l_rate = tf.constant(0.001)
delta = tf.multiply(tf.matmul(X, error, transpose_a=True), l_rate)
train = tf.assign(W, tf.add(W, delta))

sess = tf.Session()
sess.run(tf.global_variables_initializer())

epoch = 0
err = 1
while err > 0 and epoch < 5000:
    err, _ = sess.run([mse, train])
    print "mse:", err, "at epoch:", epoch
    epoch += 1
