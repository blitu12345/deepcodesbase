#!/bin/bash

echo 'install unzip'
sudo apt-get install unzip
echo 'upgrade keras'
pip install --upgrade keras
echo 'install kaglle-cli'
pip install kaggle-cli
echo 'cloning directory'
git clone https://bitbucket.org/blitu12345/deepcodesbase.git
echo 'creating data'
python create_data.py
echo 'downnload weights'
wget http://files.fast.ai/models/vgg16.h5 -P /home/ubuntu/Desktop/deepcodesbase/pretrained/data
mkdir -vp data/test/test_folder
echo 'moving files from test to test_folder'
mv data/test/*.jpg test_folder
