import numpy as np
from keras.models import Sequential, model_from_json
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.layers.core import Lambda, Flatten, Dropout, Dense, Activation
from keras import backend as K
K.set_image_dim_ordering('th')

def conv2d(model, filters, layers, kernel=(3, 3)):
    print(filters, layers, kernel)
    for layer in range(layers):
        model.add(Convolution2D(filters, kernel, padding='same', activation='relu'))
    return model

def maxpool2d(model, poolsize, strides):
    model.add(MaxPooling2D(pool_size=poolsize, strides=strides))
    return model

def fully(model, neurons):
    model.add(Dense(neurons, activation='relu'))
    return model

def dropout(model, d):
    model.add(Dropout(d))
    return model

avg_pixels = np.array([123.68, 116.779, 103.939], dtype=np.float32).reshape((3, 1, 1))

def preprocess(item):
    item = item - avg_pixels
    return item[:, ::-1]

class NeuralNets():
    def __init__(self):
        self.VGG16 = None

    def vgg16(self, chanel=3, row=224, column=224):
        model = self.VGG16 = Sequential()

        model.add(Lambda(preprocess, input_shape=(chanel, row, column), output_shape=(chanel, row, column)))
        model = conv2d(model, 64, 2)
        model = maxpool2d(model, (2, 2), (2, 2))
        model = conv2d(model, 128, 2)
        model = maxpool2d(model, (2, 2), (2, 2))
        model = conv2d(model, 256, 3)
        model = maxpool2d(model, (2, 2), (2, 2))
        model = conv2d(model, 512, 3)
        model = maxpool2d(model, (2, 2), (2, 2))
        model = conv2d(model, 512, 3)
        model = maxpool2d(model, (2, 2), (2, 2))
        model.add(Flatten())
        model = fully(model, 4096)
        model = dropout(model, 0.5)
        model = fully(model, 4096)
        model = dropout(model, 0.5)
        model.add(Dense(2, activation='softmax'))
        return model

if __name__=='__main__':
    nets = NeuralNets()
    vgg16 = nets.vgg16()
    vgg16.load_weights('data/model.h5')
    vgg16.summary()