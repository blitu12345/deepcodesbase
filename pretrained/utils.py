import pickle
import pandas as pd
from nets import *
import numpy as np
from keras import backend as K
from keras.models import Sequential, model_from_json
from keras.layers.core import Lambda, Flatten, Dense, Dropout
from keras.optimizers import SGD, RMSprop
from keras.callbacks import ModelCheckpoint
from keras.preprocessing.image import ImageDataGenerator
from keras.layers.convolutional import Convolution2D, MaxPooling2D
K.set_image_dim_ordering('th')

avg_pixels = np.array([123.68, 116.779, 103.939], dtype=np.float32).reshape((3, 1, 1))


class Vgg16(object):

    def __init__(self):
        self.image_channels = 3
        self.image_height = 224
        self.image_width = 224
        self.model = None
        self.weights = 'data/vgg16.h5'#_weights_th_dim_ordering_th_kernels
        self.createNet()

    def conv2d(self, layers, filters):
        model = self.model
        for layer in range(layers):
            # model.add(ZeroPadding2D((1, 1)))
            model.add(Convolution2D(filters, (3, 3), padding='same', activation='relu'))#
        model.add(MaxPooling2D((2, 2), strides=(2, 2)))

    def fully(self, neurals, d):
        model = self.model
        model.add(Dense(neurals, activation='relu'))
        model.add(Dropout(d))

    def createNet(self):
        model = self.model = Sequential()

        model.add(Lambda(preprocess, input_shape=(self.image_channels, self.image_height, self.image_width),
                         output_shape=(self.image_channels, self.image_height, self.image_width)))
        print('output shape', model.output_shape)
        self.conv2d(2, 64)
        self.conv2d(2, 128)
        self.conv2d(3, 256)
        self.conv2d(3, 512)
        self.conv2d(3, 512)

        model.add(Flatten())
        self.fully(4096, 0.5)
        self.fully(4096, 0.5)

        model.add(Dense(1000, activation='softmax'))
        model.load_weights(self.weights)

    def get_batches(self, path, batch_size):
        return ImageDataGenerator().flow_from_directory(
            directory=path,
            shuffle=True,
            target_size=(self.image_height, self.image_width),
            batch_size=batch_size,
            class_mode='categorical'
        )

    def compile(self):
        model = self.model
        model.compile(
            optimizer=RMSprop(1e-2),
            loss='categorical_crossentropy',
            metrics=['accuracy']
        )

    def ft(self, num):
        model = self.model
        model.layers.pop()
        model.outputs = [model.layers[-1].output]
        model.layers[-1].outbound_nodes = []
        for layer in model.layers:
            layer.trainable = False
        model.add(Dense(num, activation='softmax'))
        self.compile()

    def finetune(self, batches):
        self.ft(batches.num_class)
        classes = list(batches.class_indices)
        for _class in batches.class_indices:
            classes[batches.class_indices[_class]] = _class
        self.classes = classes

    def saveModelArchitecture(self):
        model = self.model
        print('saving model architecture')
        model_json = model.to_json()
        with open('data/model.json', 'w') as json_file:
            json_file.write(model_json)

        pickle.dump(model.get_config(), open('data/model.pickle', 'w'))


    def fit(self, train_batches, val_batches, epochs):
        model = self.model
        model.fit_generator(
            generator=train_batches,
            steps_per_epoch=train_batches.samples/train_batches.batch_size,
            nb_epoch=epochs,
            verbose=1,
            validation_data=val_batches,
            validation_steps=val_batches.samples/val_batches.batch_size
        )

    def fit_save(self, train_batches, val_batches, epochs, filepath='data/model.h5'):
        checkpoint = ModelCheckpoint(
            filepath=filepath,
            monitor='val_loss',
            verbose=1,
            save_best_only=True,
            save_weights_only=True,
            mode='min'
        )
        model = self.model
        model.fit_generator(
            generator=train_batches,
            steps_per_epoch=train_batches.samples/train_batches.batch_size,
            nb_epoch=epochs,
            verbose=1,
            validation_data=val_batches,
            validation_steps=val_batches.samples/val_batches.batch_size,
            callbacks=[checkpoint]
        )


def preprocess(item):
    item = item - avg_pixels
    return item[:, ::-1]


def prediction(batch_size=64):
    model = NeuralNets().vgg16()
    model.summary()
    model.load_weights('data/model.h5')
    gen = ImageDataGenerator().flow_from_directory(
        'data/test',
        shuffle=False,
        target_size=(224, 224),
        batch_size=batch_size,
        class_mode=None
    )
    preds = model.predict_generator(
        generator=gen,
        steps=gen.samples/gen.batch_size + 1,
        verbose=1
    )
    # print([pred for pred in preds[:, 1]][:5])
    # print preds
    # for pred in preds[:5]:
    #     print "pred", pred, pred[:, 1]
    pred = np.asarray([np.clip(round(pred, 4), 0.5, 0.95) for pred in preds[:, 1]])
    files = [file.split('.')[0].split('/')[1] for file in gen.filenames]
    assert len(files) == len(pred)
    df = pd.DataFrame({'id': files, 'label': np.asarray(pred)})
    df.to_csv('sub1.csv', index=False)

# if __name__ == '__main__':
#     # model = Vgg16()
#     # train_batches = model.get_batches(path='data/train', batch_size=64)
#     # model.finetune(train_batches)
#     # model.model.summary()
#     # model.model.save_weights(filepath='data/model.h5')
#     # val_batches = model.get_batches(path='data/val', batch_size=64)
#     # model.fit_save(train_batches, val_batches, 2)
#     # model.saveModelArchitecture()
#     print(prediction())
