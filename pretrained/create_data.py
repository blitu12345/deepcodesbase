from __future__ import print_function

import os
from six.moves import urllib
from os.path import join


class CreateData:

    def __init__(self):
        self.current_path = os.getcwd()
        self.data_path = join(self.current_path, 'data')
        self.train_path = None
        self.test_path = None
        self.val_path = None

    def ProcessDirectory(self):
        if True:
            os.system('mkdir -vp {}'.format(self.data_path))
            sample_url = 'https://www.kaggle.com/c/dogs-vs-cats-redux-kernels-edition/download/sample_submission.csv'
            print("enter pass to download data")
            os.system('kg download -u ashishbkarel@gmail.com -p {} -c dogs-vs-cats-redux-kernels-edition'.format(raw_input()))
            print("train data download started")
            print("test data download started")
            print("sample file download started")
            # _, _ = urllib.request.urlretrieve(sample_url, 'data/sample.csv')

            print('unzip train and test files')
            os.system('unzip {0} -d {1}'.format('train.zip', self.data_path))
            os.system('unzip {0} -d {1}'.format('test.zip', self.data_path))

        print("create train and test files if not found already")
        self.train_path = join(self.data_path, 'train')
        self.test_path = join(self.data_path, 'test/test_folder')
        self.val_path = join(self.data_path, 'val')
        if len(os.listdir(self.current_path)) != 3:
            os.system('mkdir -vp {}'.format(self.train_path))
            os.system('mkdir -vp {}'.format(self.test_path))
            os.system('mkdir -vp {}'.format(self.val_path))

        cats_path = join(self.train_path, 'cats')
        dogs_path = join(self.train_path, 'dogs')

        print("creating cats and dogs files in train")
        if not os.path.exists(cats_path):
            os.system('mkdir -vp {}'.format(cats_path))
        if not os.path.exists(dogs_path):
            os.system('mkdir -vp {}'.format(dogs_path))

        print("moving the cats and dogs into cats andf dogs folder in ")
        print('mv {0}/cat.*.jpg {1}'.format(self.train_path, cats_path))
        print('mv {0}/dog.*.jpg {1}'.format(self.train_path, dogs_path))
        os.system('mv {0}/cat.*.jpg {1}'.format(self.train_path, cats_path))
        os.system('mv {0}/dog.*.jpg {1}'.format(self.train_path, dogs_path))

        if os.path.exists(self.val_path):
            os.system("mkdir -vp {}".format(self.val_path))
        cats_val_path = join(self.val_path, 'cats')
        dogs_val_path = join(self.val_path, 'dogs')
        if not os.path.exists(cats_val_path):
            os.system('mkdir -vp {}'.format(cats_val_path))
        if not os.path.exists(dogs_val_path):
            os.system('mkdir -vp {}'.format(dogs_val_path))

        os.system('mv {0}/dog.*1.jpg {1}'.format(dogs_path, dogs_val_path))
        os.system('mv {0}/cat.*1.jpg {1}'.format(cats_path, cats_val_path))



if __name__ == '__main__':
    cd = CreateData()
    cd.ProcessDirectory()
