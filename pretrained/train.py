import numpy as np
import argparse
from utils import *

parser = argparse.ArgumentParser()
parser.add_argument('--epochs', type='int', default=1)

args = parser.parse_args()

vgg = Vgg16()
train_batches = vgg.get_batches('data/train', batch_size=32)
val_batches = vgg.get_batches('data/val', batch_size=32)
vgg.finetune(train_batches)
vgg.model.summary()
vgg.fit_save(train_batches, val_batches, args.epochs)
