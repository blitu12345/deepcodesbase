import matplotlib
matplotlib.use('Agg')
from keras.preprocessing.image import ImageDataGenerator
import os
from os.path import join
import numpy as np
import matplotlib.pyplot as plt
from keras import backend as K
from keras import layers

def train_datagen():
    return ImageDataGenerator(
        rotation_range=40,
        rescale=1/255.0,
        width_shift_range=0.1,
        height_shift_range=0.1,
        zoom_range=0.2,
        shear_range=0.2,
        horizontal_flip=True,
        fill_mode='nearest'
    )


def val_datagen():
    return ImageDataGenerator(
        rescale=1/255.
    )


def train_generator(path=None, gen=ImageDataGenerator(1/255.), height=94, width=94, batch_size=50):

    if path is None:
        path = join(os.getcwd(), 'data/train')

    return gen.flow_from_directory(
        path,
        target_size=(height, width),
        batch_size=batch_size,
        class_mode='binary'
    )


def val_generator(path=None, gen=ImageDataGenerator(1/255.), height=94, width=94, batch_size=50):

    if path is None:
        path = join(os.getcwd(), 'val')

    return gen.flow_from_directory(
        path,
        target_size=(height, width),
        batch_size=batch_size,
        class_mode='binary'
    )

def visualize(history=None, *args):

    plt.figure(figsize=(15, 10))
    colors = ['r', 'g', 'b', 'y']
    title = ""
    for color, arg in zip(colors, args):
        plt.plot(history.history[arg], label=str(arg), color=color)
        title += str(arg) + ","
    plt.xlabel("iterations")
    plt.title("plot of {}".format(title))
    plt.legend(loc=9, prop={'size': 8})
    plt.savefig('learning_curve')

    return None

avg_pixels = np.array([103.939, 116.779, 123.68])


def pre_process(item):
    item = item - avg_pixels
    return item[:, ::, ::-1]


class MyLayer(layers.Layer):
    
    def __init__(self, input_shape, **kwargs):
        self.inShape = input_shape
        self.avg_pixels = np.array([103.939, 116.779, 123.68])
        super(MyLayer, self).__init__(**kwargs)

    def call(self, x, mask=None):
        x = x - avg_pixels
        return x[:, ::, ::-1]

    def get_output_shape_for(self, input_shape):
        return self.inShape





























































