
from keras.models import Sequential
from keras.layers import Conv2D, Dropout, Flatten, Dense, MaxPooling2D, Activation
from keras.optimizers import SGD, RMSprop
from keras.layers.core import Lambda
from utils import *


class Model(object):

    def __init__(self, channels=None, width=None, height=None):
        self.channels = channels
        self.width = width
        self.height = height

    def net_lecun(self):
        model = Sequential()
        model.add(Conv2D(32, (3, 3), input_shape=(self.height, self.width, self.channels)))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2,2)))

        model.add(Conv2D(32, (3, 3)))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(64, (3, 3)))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Flatten())
        model.add(Dense(64))
        model.add(Activation('relu'))
        model.add(Dropout(0.25))
        model.add(Dense(1))

        model.add(Activation('sigmoid'))

        lr = 0.001
        sgd = SGD(lr=lr, decay=1e-6, momentum=0.9, nesterov=True)
        model.compile(loss='binary_crossentropy',
                      optimizer=RMSprop(lr=1e-4),
                      metrics=['accuracy']
        )
        return model


    def VGG16(self):

        model = Sequential()

        model.add(Lambda(pre_process, input_shape=(self.height, self.width, self.channels)))
   #     model.add(MyLayer(input_shape=(self.height, self.width, self.channels)))
        model.add(Conv2D(64, (3, 3), input_shape=(self.height, self.width, self.channels), padding="same",\
                         activation='relu'))
        model.add(Conv2D(64, (3, 3), padding="same", activation='relu'))
        model.add(MaxPooling2D(2, 2))

        model.add(Conv2D(128, (3, 3), padding="same", activation='relu'))
        model.add(Conv2D(128, (3, 3), padding="same", activation='relu'))
        model.add(MaxPooling2D((2, 2)))

        model.add(Conv2D(256, (3, 3), padding="same", activation='relu'))
        model.add(Conv2D(256, (3, 3), padding="same", activation='relu'))
        model.add(Conv2D(256, (3, 3), padding="same", activation='relu'))
        model.add(MaxPooling2D(2, 2))

        model.add(Conv2D(512, (3, 3), padding="same", activation='relu'))
        model.add(Conv2D(512, (3, 3), padding="same", activation='relu'))
        model.add(Conv2D(512, (3, 3), padding="same", activation='relu'))
        model.add(MaxPooling2D((2, 2)))

        model.add(Conv2D(512, (3, 3), padding="same", activation='relu'))
        model.add(Conv2D(512, (3, 3), padding="same", activation='relu'))
        model.add(Conv2D(512, (3, 3), padding="same", activation='relu'))
        model.add(MaxPooling2D((2, 2)))

        model.add(Flatten())
        model.add(Dense(4096))
        model.add(Dropout(0.5))
        model.add(Dense(4096))
        model.add(Dropout(0.5))
        model.add(Dense(1))

        model.add(Activation('sigmoid'))
        lr = 0.001
        model.compile(loss='binary_crossentropy',
                      optimizer=RMSprop(lr=1e-4),
                      metrics=['accuracy']
        )
        model.summary()

        return model


if __name__ == "__main__":
    model = Model(3, 64, 64)
    print("successfully compiled")
