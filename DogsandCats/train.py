
from __future__ import print_function
import argparse
from utils import *
from convnet import *
from keras.callbacks import ModelCheckpoint

parser = argparse.ArgumentParser()
parser.add_argument('--batch_size', type=int, help='batch_size', default=50)
parser.add_argument('--epochs', type=int, help='epoch', default=50)
parser.add_argument('--vis', action='store_true', help='want ot visualise')
parser.add_argument('--test', action='store_true', help='want to test model')
parser.add_argument('--h', default=64, type=int, help='height of input image')
parser.add_argument('--w', default=64, type=int, help='width of input image')
parser.add_argument('--train', action='store_true', help='train the model')
parser.add_argument('--net', default='VGG16', help='either vgg or yann lecun')
parser.add_argument('--channels', default=3, type=int, help='channels in images')
args = parser.parse_args()


model = Model(channels=3, height=224, width=224)

if args.net in ['VGG16', 'VGG', 'vgg']:
    model = model.VGG16()
else:
    model = model.net_lecun()

# data generator
if args.train:

    print('loading train data generator\n')
    train_gen = train_generator(path='data/train/', gen=train_datagen(), height=224, width=224,\
                                batch_size=args.batch_size)
    print("train data generator completed\n")

    print('loading val data generator\n')
    val_gen = val_generator(path='data/val/', gen=val_datagen(), height=224, width=224, batch_size=args.batch_size)
    print('val data generator ready')

    print('model summary')
    print(model.summary())

    file_path = 'model_best.hdf5'
    checkpoint = ModelCheckpoint(file_path, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
    print('model_checkpoint ready\n')
    callbacks = [checkpoint]
    print('started fitting model \n')

    history = model.fit_generator(train_gen, nb_epoch=args.epochs, steps_per_epoch=int(24778/args.batch_size)+1,
                                  verbose=1, callbacks=callbacks, validation_data=val_gen,
                                  validation_steps=int(222/args.batch_size)+1
                                  )

print("loss", history.history['loss'])
print('val loss', history.history['val_loss'])
if args.vis:
    print("visualising")
    visualize(history, 'loss', 'val_loss')
